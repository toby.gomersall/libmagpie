#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

int main() {
    int fd;
    int buffer_size = 16;
    uint8_t write_buffer[buffer_size];
    uint8_t read_buffer[buffer_size];
    int i;
    int ret;

    fd = open("/dev/axi_control_0", O_RDWR);

    printf("Please confirm that the LEDs flash as if counting up to 5 in "
            "binary\n");

    // Clear the registers
    for(i = 0; i < buffer_size; i++){
        write_buffer[i] = 0x00;
    }
    lseek(fd, 0, SEEK_SET);
    write(fd, &write_buffer, buffer_size);

    // Flash the LEDs in a binary count sequence
    for(i = 0; i <= 4; i++){

        write_buffer[0] = i+1;
        lseek(fd, 0, SEEK_SET);
        write(fd, &write_buffer, 1);

        sleep(1);

        write_buffer[0] = 0x00;
        lseek(fd, 0, SEEK_SET);
        write(fd, &write_buffer, 1);

        sleep(1);
    }

    // Write data to the registers
    for(i = 0; i < 4; i++){
        write_buffer[i] = (uint8_t) i + 1;
    }
    lseek(fd, 6, SEEK_SET);
    write(fd, &write_buffer, 4);

    // Write data to the registers
    for(i = 0; i < 4; i++){
        write_buffer[i] = (uint8_t) i + 8;
    }
    lseek(fd, 12, SEEK_SET);
    write(fd, &write_buffer, 4);

    // Check that the values read back are the expected values.
    // The last byte should be changed because I have tweaked the VHDL to add
    // 1 to any values stored in the register. This was to ensure that the 
    // data is being written to and read back from the PL.
    lseek(fd, 0, SEEK_SET);
    read(fd, &read_buffer, 16);
    if ((read_buffer[0] == 0x01) && (read_buffer[1] == 0x00) && 
            (read_buffer[2] == 0x00) && (read_buffer[3] == 0x00) && 
            (read_buffer[4] == 0x01) && (read_buffer[5] == 0x00) && 
            (read_buffer[6] == 0x01) && (read_buffer[7] == 0x02) && 
            (read_buffer[8] == 0x04) && (read_buffer[9] == 0x04) && 
            (read_buffer[10] == 0x00) && (read_buffer[11] == 0x00) && 
            (read_buffer[12] == 0x09) && (read_buffer[13] == 0x09) && 
            (read_buffer[14] == 0x0A) && (read_buffer[15] == 0x0B))
        ret = 0;
    else
        ret = -1;

    close(fd);

    return ret;
}
