#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

int main() {
    int fd, fd_1;
    int size = 4096;
    int *mem, *mem_1;
    int i;

    int ret;

    // Open device and check for errors
    fd = open("/dev/sa_ac0", O_RDWR);
    mem = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem == MAP_FAILED) {
        perror("ERROR <Memory Map Failed> could not mmap memory\n");
        return -1;
    }

    // Open device and check for errors
    fd_1 = open("/dev/sa_ac1", O_RDWR);
    mem_1 = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED, fd_1, 0);
    if (mem_1 == MAP_FAILED) {
        perror("ERROR <Memory Map Failed> could not mmap memory 1\n");
        return -1;
    }

    printf("Please confirm LEDs are flash 5 times following this pattern:\n"
            "|1|1|1|1|1|0|1|0|\n"
            "|0|0|0|0|0|1|0|1|\n");

    // Loop round and flash the LEDs
    for(i == 0; i <= 4; i++) {
        mem[0] = 0x0000000A;
        mem_1[0] = 0x0000000F;

        sleep(1);

        mem[0] = 0x00000005;
        mem_1[0] = 0x00000000;

        sleep(1);
    }

    // Write some data to memory
    mem[0] = 0x99999999;
    mem[1] = 0xAAAAAAAA;
    mem[2] = 0x33333333;
    mem[3] = 0x55555555;
    mem[4] = 0xBBBBBBBB;

    // Check that the values read back are the expected values.
    // The last byte should be changed because I have tweaked the VHDL to add
    // 1 to any values stored in the register. This was to ensure that the 
    // data is being written to and read back from the PL.
    if ((mem[0] == 0xBBBBBBBC) && (mem[1] == 0xAAAAAAAB) && 
            (mem[2] == 0x33333334) && (mem[3] == 0x55555556))
        ret = 0;
    else
        ret = -1;

    if (munmap(mem, size) != 0) {
        perror("Error un-mmapping the memory");
        return -1;
    }

    if (munmap(mem_1, size) != 0) {
        perror("Error un-mmapping the memory 1");
        return -1;
    }

    close(fd);
    close(fd_1);

    return ret;
}
